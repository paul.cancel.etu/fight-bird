package fightbird.model.items;

public class Dice {
    private static final int MAX_DICE_NUMBER = 20;

    public Dice(){}

    public static boolean fight(){
        return (playerRoll()>computerRoll());
    }

    private static int roll(int nb){
        return (int)(Math.random()*nb)+1;
    }

    private static int playerRoll() {
        int playerRoll;
        playerRoll = roll(MAX_DICE_NUMBER);
        printRoll(playerRoll);
        return playerRoll;
    }

    private static int computerRoll() {
        int computerRoll;
        computerRoll = roll(MAX_DICE_NUMBER);
        printRoll(computerRoll);
        return computerRoll;
    }

    private static void printRoll(int roll) {
        System.out.println("Your ennemi start rolling the dice");
        System.out.println("----------");
        System.out.println("He roll a "+roll+" !");
    }
}
