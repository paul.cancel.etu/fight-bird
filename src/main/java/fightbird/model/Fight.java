package fightbird.model;

import java.util.Scanner;

import fightbird.model.entities.*;
import fightbird.model.items.Item;
import fightbird.model.items.Magic;
import fightbird.model.items.Range;

public class Fight {
    private Player player;
    private Enemy enemy;
    private boolean isFinished;

    public boolean isFinished() {
        return isFinished;
    }

    public Fight(Player player, Enemy enemy) {
        this.player = player;
        this.enemy = enemy;
        this.isFinished = false;
    }

    public boolean canAttack(Item usedItem){
        if(usedItem.getType() == Type.MAGIC){
            Magic magicItem = (Magic) usedItem;
            return player.getMana_point()>= magicItem.getUse_mana();
        }
        if(usedItem.getType()== Type.RANGE){
            Range rangeItem = (Range) usedItem;
            return player.getAmmo() >= rangeItem.getUse_ammo();
        }
        return true;
    }

    public void playerAttack(Item usedItem){
        if(this.canAttack(usedItem)){
            if(usedItem.getType() == Type.MAGIC){
            Magic magicItem = (Magic) usedItem;
            player.decreaseMana(magicItem.getUse_mana());
        }
        if(usedItem.getType()== Type.RANGE){
            Range rangeItem = (Range) usedItem;
            player.decreaseAmmo(rangeItem.getUse_ammo());
        }
            int damage = usedItem.damageTotal(player.getAttack_value(), enemy);
            enemy.takeDamage(damage);
            System.out.println("you hit "+ damage);
        }
    }

    public void enemyAttack(){
        int damage = enemy.getAttack();
        player.takeDamage(damage);
        System.out.println("you get hit "+ damage);
    }

    public void fight(Item usedItem){
        Visuals.clear();
        Visuals.printFight();
        this.playerAttack(usedItem);
        if(enemy.getPv()>0){
            this.enemyAttack();
        }
        if(enemy.getPv()<=0 || player.getHealth_point()<=0){
            isFinished=true;
        }
    }
    
    public static void fight(Player player, Enemy enemy){
        Fight fight = new Fight(player, enemy);
        
        Scanner sc = new Scanner(System.in);
        Visuals.randomSpeech(enemy);
        String select = "";
        player.getInventory().openInventory();
        while(!fight.isFinished()){
            visuals(player, enemy);
            select = sc.nextLine();
            switch (select) {
                case "1":
                    fight.fight(player.getInventory().getChoice()[0]);
                    break;
                case "2":
                    fight.fight(player.getInventory().getChoice()[1]);
                    break;  
                case "3":
                    fight.fight(player.getInventory().getChoice()[2]);
                    break;
                case "4":
                    fight.fight(player.getInventory().getChoice()[3]);
                    player.getInventory().getInventory().remove(player.getInventory().getChoice()[3]);
                    break;
                default:
                    break;
            }
            Visuals.wait(sc);
        }
    }

    private static void visuals(Player player, Enemy enemy) {
        StringBuilder sb = new StringBuilder();
        Visuals.clear();
        if(enemy.getClass() == Boss.class){
            Visuals.printBoss();
        }
        else {
            Visuals.printFight();
        }
        sb.append(" \n Player status :");
        sb.append(player);
        sb.append(" \n enemy status:");
        sb.append(enemy);
        sb.append("Select weapon : 1 melee 2 range 3 magic 4 special");
        System.out.println(sb.toString());
    }
    
}
