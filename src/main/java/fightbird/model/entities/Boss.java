package fightbird.model.entities;

public class Boss extends Enemy{
    private static final int SCORE_MULTIPLIER = 2;
    private static final int ATTACK_MULTIPLIER = 3;
    private static final int HP_MULTIPLIER = 8;

    public Boss(String name, int pVMAX, int attack) {
        super(name, pVMAX*HP_MULTIPLIER, attack*ATTACK_MULTIPLIER);
    }

    public Boss(int score){
        super(score * SCORE_MULTIPLIER);
    }
    

}
