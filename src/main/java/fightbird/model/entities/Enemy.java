package fightbird.model.entities;


public class Enemy {
    private static final int SCORE_DIVIDER = 100;
    private static final int DEFAULT_HEALTH = 20;
    private static final int DEFAULT_ATTACK = 1;
    private static final int NUMBEROFNAMES = 10;
    private static final int MINPVMAX = 10;
    private static final int MINATTACK = 10;


    private String name;
    private int pv;
    final private int PVMAX;
    private int attack;


    private int randomStat;

    private static final String[] NAMES = {"Piaf-Piaf","Pirou-Pirou","Pac-Man","Pouf-Pouf","Pif-Pif","Pouet-Pouet","Abracadabra","Lorem ipsum","Gandalf","Hello World"};
    
    public Enemy(String name, int pVMAX, int attack) {
        this.name = name;
        this.pv = pVMAX;
        this.PVMAX = pVMAX;
        this.attack = attack;
    }

    public Enemy(int score){
        this.name = NAMES[(int) (Math.random()*NUMBEROFNAMES)];
        randomStat = (int) (Math.random()*(score+1));
        this.pv = MINPVMAX+(score*score) + (randomStat*2) * (int) ((score/SCORE_DIVIDER)+1);
        this.PVMAX = this.pv;
        this.attack = MINATTACK + (score - randomStat) * (int) ((score/SCORE_DIVIDER)+1); 

    }

    public Enemy(){
        this(NAMES[(int) (Math.random()*NUMBEROFNAMES)],DEFAULT_HEALTH,DEFAULT_ATTACK);
    }

    public int getPVMAX() {
        return this.PVMAX;
    }
    public String getName() {
        return name;
    }
    
    public int getPv() {
        return pv;
    }
    
    public int getAttack() {
        return attack;
    }
        
    public void takeDamage(int damage){
        if(pv-damage<0){
            pv=0;
        }
        else {
            pv-=damage;
        }
    }

    @Override
    public String toString() {
        return "Name :" + name + "\npv : " + pv + "/" + PVMAX + "\nattack : " + attack ;
    }
}
