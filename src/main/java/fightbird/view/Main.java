package fightbird.view;

import fightbird.model.*;
import fightbird.model.entities.*;
import fightbird.model.items.*;

import java.util.Scanner;

public class Main {

    private static void menu(){
        Visuals.clear();
        Visuals.printMenu();
    }

    public static void main(String[] args) {
        Player user = new Player();
        Enemy ennemy = new Enemy();
        Scanner sc = new Scanner(System.in);
        String select = "";
        boolean end = false;
        Map map;
        while (!end) {
            user = new Player();
            int rarity = 1;
            int level = 1;
            ennemy = new Enemy();
            menu();
            System.out.println(" Your Choice : ");
            select = sc.nextLine();
            Visuals.clear();
            switch (select) {
                case "1":
                    Visuals.lore();
                    Visuals.wait(sc);
                    while(user.getHealth_point()>0){
                        Visuals.clear();
                        map = new Map();
                        if(map.flappy(rarity,user)){

                            if (level%5 == 0) {
                                ennemy = new Boss(level);
                            } else {
                                ennemy = new Enemy(level);
                                
                            }
                            Fight.fight(user, ennemy);
                            System.out.flush();
                            if(ennemy.getPv()<=0){
                                Visuals.clear();
                                level+=1;
                                user.scaleUp(level);
                                user.heal();
                                if(level%5==0){
                                    if(rarity<5){
                                        rarity++;
                                    }
                                    System.out.println(ImportWeapons.dropSpecial(user));
                                    Visuals.wait(sc);
                                }
                                Visuals.clear();
                            }
                        }
                        else{
                            user.takeDamage(100000000);
                        }
                    }
                    RecordManager.changeScore(level);
                    Visuals.printGameOver();
                    Visuals.wait(sc);
                    break;
                case "2":
                    System.out.println("Le meilleur score c'est "+RecordManager.getBestScore());
                    Visuals.wait(sc);
                    break;
                case "3":
                    rules();
                    break;
                case "0":
                    end =true;
                    break;
                default:
                    break;
                }
            Visuals.clear();
            
        }
        
    }

    public static void rules(){
        Visuals.printRules();
        Scanner sc = new Scanner(System.in);
        Visuals.wait(sc);
    }
}
